<!Doctype html>
<html>
    <head>
        <style>
            .float-left{
                float: left;
            }
            .clear{
                clear: both;
            }
        </style>        
    </head>
    <body>        
        <div>
            <span class="float-left" id="maleFemaleRatio" style="width: 300px; height: 300px;"></span>
            <span class="float-left" id="activeEmployee" style="width: 300px; height: 300px;"></span>
            <span class="float-left" id="leftEmployee" style="width: 300px; height: 300px;"></span>
            <span class="float-left" id="joinedEmployee" style="width: 300px; height: 300px;"></span>
        </div>
        <div class="clear"></div>
        <div>
            <span class="float-left" id="loanAmount" style="width: 300px; height: 300px;"></span>
            <span class="float-left" id="loanReturnAmount" style="width: 300px; height: 300px;"></span>
            <span class="float-left" id="remainingLoan" style="width: 300px; height: 300px;"></span>            
        </div>
        <div class="clear"></div>
        <div>
            <span id="loanReturnRemaining" style="width: 600px; height: 600px;"></span>
        </div>
        <div class="clear"></div>
        <div style="width: 600px; height: 400px;">
            <canvas id="attandenceSummary"></canvas>
        </div>
        <div style="width: 1000px; height: 400px;">
            <div id="attandenceSummary2"></div>
        </div>        
        
        <!--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>-->
        <script type="text/javascript" src="charts-loader.js"></script>
        <script type="text/javascript" src="chart.bundle.js"></script>        
        
        <script>
            //rendering google chart
            google.charts.load('current', {
                'packages': ['corechart']
            });
            google.charts.setOnLoadCallback(drawMaleFemaleRatio);
            google.charts.setOnLoadCallback(drawActiveEmployee);
            google.charts.setOnLoadCallback(drawLeftEmployee);
            google.charts.setOnLoadCallback(drawJoinedEmployee);
            google.charts.setOnLoadCallback(drawLoanAmount);
            google.charts.setOnLoadCallback(drawLoanReturnAmount);
            google.charts.setOnLoadCallback(drawRemainingLoanAmount);
            google.charts.setOnLoadCallback(drawLoanReturnRemaining);
            
            //male female ratio starts (pie chart)
            function drawMaleFemaleRatio() {
                //setting data
                var data = google.visualization.arrayToDataTable([
                    ['Task', 'Male Female Ratio'],
                    ['Male', 720],
                    ['Female', 689]
                ]);
                var options = {
                    title: 'Male Female Employee Ratio'
                };
                var maleFemaleChart = new google.visualization.PieChart(document.getElementById('maleFemaleRatio'));
                maleFemaleChart.draw(data, options);
            }
            //male female ratio ends
            
            //active employee starts (counting chart i.e. same as pie chart)
            function drawActiveEmployee() {
                //setting data
                var data = google.visualization.arrayToDataTable([
                    ['Task', 'Active Employee'],
                    ['', 1409]                    
                ]);
                var options = {
                    title: 'Active Employee Info',
                    legend: 'none',
                    pieSliceText: 'value',
                    tooltip: { trigger: 'none' },
                    slices: {
                        0:{color: 'yellow'}
                    },
                    pieSliceTextStyle: {
                        color: 'black',
                        fontSize: 14,
                        bold: 1
                    }
                };
                var activeEmployeechart = new google.visualization.PieChart(document.getElementById('activeEmployee'));
                activeEmployeechart.draw(data, options);
            }
            //active employee ends
            
            //left employee starts
            function drawLeftEmployee() {
                //setting data
                var data = google.visualization.arrayToDataTable([
                    ['Task', 'Left Employee'],
                    ['', 3]                    
                ]);
                var options = {
                    title: 'Left Employee',
                    legend: 'none',
                    pieSliceText: 'value',
                    tooltip: { trigger: 'none' },
                    slices: {
                        0:{color: 'red'}
                    },
                    pieSliceTextStyle: {    
                        color: 'black',
                        fontSize: 14,
                        bold: 1
                    }
                };
                var leftEmployeeChart = new google.visualization.PieChart(document.getElementById('leftEmployee'));
                leftEmployeeChart.draw(data, options);
            }
            //left employee ends
            //joined employee starts
             function drawJoinedEmployee() {
                //setting data
                var data = google.visualization.arrayToDataTable([
                    ['Task', 'Joined Employee'],
                    ['', 147]                    
                ]);
                var options = {
                    title: 'Joined Employee',
                    legend: 'none',
                    pieSliceText: 'value',
                    tooltip: { trigger: 'none' },
                    slices: {
                        0:{color: 'lime'}
                    },
                    pieSliceTextStyle: {
                        color: 'black',
                        fontSize: 14,
                        bold: 1
                    }                    
                };
                var joinedEmployeeChart = new google.visualization.PieChart(document.getElementById('joinedEmployee'));
                joinedEmployeeChart.draw(data, options);
            }
            //joined employee ends
            //loan amount starts
            function drawLoanAmount() {
                //setting data
                var data = google.visualization.arrayToDataTable([
                    ['Task', 'Loan Amount'],
                    ['', 928500.00]                    
                ]);
                var options = {
                    title: 'Loan Amount',
                    legend: 'none',
                    pieSliceText: 'value',
                    tooltip: { trigger: 'none' },
                    slices: {
                        0:{color: 'red'}
                    },
                    pieSliceTextStyle: {     
                        color: 'black',
                        fontSize: 14,
                        bold: 1
                    }
                };
                var leftEmployeeChart = new google.visualization.PieChart(document.getElementById('loanAmount'));
                leftEmployeeChart.draw(data, options);
            }
            //loan amount ends
            //loan return amount starts
            function drawLoanReturnAmount() {
                //setting data
                var data = google.visualization.arrayToDataTable([
                    ['Task', 'Loan Return Amount'],
                    ['', 259500.00]                    
                ]);
                var options = {
                    title: 'Loan Return Amount',
                    legend: 'none',
                    pieSliceText: 'value',
                    tooltip: { trigger: 'none' },
                    slices: {
                        0:{color: 'lime'}
                    },
                    pieSliceTextStyle: {     
                        color: 'black',
                        fontSize: 14,
                        bold: 1
                    }
                };
                var leftEmployeeChart = new google.visualization.PieChart(document.getElementById('loanReturnAmount'));
                leftEmployeeChart.draw(data, options);
            }
            //loan return amount ends
            //loan return amount starts
            function drawRemainingLoanAmount() {
                //setting data
                var data = google.visualization.arrayToDataTable([
                    ['Task', 'Remaining Loan Amount'],
                    ['', 669000.00]                    
                ]);
                var options = {
                    title: 'Remaining Loan Amount',
                    legend: 'none',
                    pieSliceText: 'value',
                    tooltip: { trigger: 'none' },
                    slices: {
                        0:{color: 'yellow'}
                    },
                    pieSliceTextStyle: {     
                        color: 'black',
                        fontSize: 14,
                        bold: 1
                    }
                };
                var leftEmployeeChart = new google.visualization.PieChart(document.getElementById('remainingLoan'));
                leftEmployeeChart.draw(data, options);
            }
            //loan return amount ends
            //Loan Return Remaining starts (pie chart)
            function drawLoanReturnRemaining() {
                //setting data
                var data = google.visualization.arrayToDataTable([
                    ['Task', 'Loan - Return - Remaining'],
                    ['Loan', 928500.00],
                    ['Return', 259500.00],
                    ['Remaining', 669000.00]
                ]);
                var options = {
                    title: 'Loan Info'
                };
                var maleFemaleChart = new google.visualization.PieChart(document.getElementById('loanReturnRemaining'));
                maleFemaleChart.draw(data, options);
            }
            //Loan Return Remaining ends
            //Bar Chart Starts
            var ctx = document.getElementById("attandenceSummary").getContext("2d");
            var data = {
              labels: ["Chocolate", "Vanilla", "Strawberry", "Pinapple"],
              datasets: [{
                label: "Blue",
                backgroundColor: "blue",
                data: [300, 700, 400, 50]
              }, {
                label: "Red",
                backgroundColor: "red",
                data: [400, 300, 500, 97]
              }, {
                label: "Green",
                backgroundColor: "green",
                data: [700, 200, 600, 609]
              }, {
                label: "Yellow",
                backgroundColor: "yellow",
                data: [800, 289, 650, 120]
              }]
            };
            var myBarChart = new Chart(ctx, {
              type: 'bar',
              data: data,
              options: {
                barValueSpacing: 10,
                scales: {
                  yAxes: [{
                    ticks: {
                      min: 0,
                    }
                  }]
                }
              }
            });
            //Bar Chart Ends            
            //Bar Chart 2 Starts
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawAttandenceSummary2);

            function drawAttandenceSummary2() {
              // Some raw data (not necessarily accurate)
              var data = google.visualization.arrayToDataTable([
               ['', 'Strength', 'Present', 'Absent', 'Leave'],
               ['Admin',  165,      938,         522,             998           ],
               ['Cutting',  135,      1120,        599,             1268          ],
               ['Finishing',  157,      1167,        587,             807           ],
               ['Maintenance',  139,      1110,        615,             968           ],
               ['Office',  136,      691,         629,             1026          ],
               ['Production',  136,      691,         629,             1026          ],
               ['Sample',  136,      691,         629,             1026          ],
               ['Sewing',  136,      691,         629,             1026          ],
               ['Store',  136,      691,         629,             1026          ],
               ['Washing',  136,      691,         629,             1026          ]
                ]);

              var options = {
                title : 'Attendance Summery',
                seriesType: 'bars',
                colors: ['blue','green','orange','red'],
                series: {4: {type: 'line'}}
              };

              var chart = new google.visualization.ComboChart(document.getElementById('attandenceSummary2'));
              chart.draw(data, options);
            }

            //Bar Chart 2 Ends
        </script>
    </body>
</html>